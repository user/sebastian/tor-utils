#!/bin/bash

## See LICENSE for copyright and licensing information

# Set some error reporting stuff
set -e
set -u

if [ $(whoami) = "root" ]
then
  echo "Running as root is not supported."
  exit 1
fi


source ./config

WD="`pwd`/work"
PIDDIR="$WD/pid"

